parser grammar ZshParser;

//https://pubs.opengroup.org/onlinepubs/9699919799/utilities/V3_chap02.html#tag_18

options { tokenVocab=ZshLexer; }

prog: stat+ ;

stat: expr NEWLINE
| ID EQUAL expr NEWLINE
| NEWLINE
;

expr : ID (ID)+
| '(' expr ')'
;

